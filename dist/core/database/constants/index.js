"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Role = exports.PROJECT_REPOSITORY = exports.PROJECT_CONTRIBUTOR_REPOSITORY = exports.POST_REPOSITORY = exports.USER_REPOSITORY = exports.PRODUCTION = exports.TEST = exports.DEVELOPMENT = exports.SEQUELIZE = void 0;
exports.SEQUELIZE = 'SEQUELIZE';
exports.DEVELOPMENT = 'development';
exports.TEST = 'test';
exports.PRODUCTION = 'production';
exports.USER_REPOSITORY = 'USER_REPOSITORY';
exports.POST_REPOSITORY = 'POST_REPOSITORY';
exports.PROJECT_CONTRIBUTOR_REPOSITORY = 'PROJECT_CONTRIBUTOR_REPOSITORY';
exports.PROJECT_REPOSITORY = 'PROJECT_REPOSITORY';
var Role;
(function (Role) {
    Role["User"] = "user";
    Role["Admin"] = "admin";
})(Role = exports.Role || (exports.Role = {}));
//# sourceMappingURL=index.js.map