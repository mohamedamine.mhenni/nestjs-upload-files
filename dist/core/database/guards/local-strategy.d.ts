import { Strategy } from 'passport-local';
import { AuthService } from '../../../modules/auth/auth.service';
import { ConfigService } from '@nestjs/config';
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private readonly authService;
    private configService;
    constructor(authService: AuthService, configService: ConfigService);
    validate(username: string, password: string): Promise<any>;
}
export {};
