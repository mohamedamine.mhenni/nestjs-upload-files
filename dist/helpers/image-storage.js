"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveImageToStorage = void 0;
const multer_1 = require("multer");
const uuid_1 = require("uuid");
const fs = require('fs');
const FileType = require('file-type');
const path = require("path");
const common_1 = require("@nestjs/common");
const validFileExtensions = ['png', 'jpeg', 'jpg'];
const validMimeTypes = [
    'image/png',
    'image/jpg',
    'image/jpeg',
];
exports.saveImageToStorage = {
    storage: (0, multer_1.diskStorage)({
        destination: './images',
        filename: (req, file, cb) => {
            const fileExtension = path
                .extname(file.originalname)
                .toLowerCase();
            const fileName = (0, uuid_1.v4)() + fileExtension;
            cb(null, fileName);
        },
    }),
    fileFilter: (req, file, cb) => {
        const allowedMimeTypes = validMimeTypes;
        allowedMimeTypes.includes(file.mimetype)
            ? cb(null, true)
            : cb(new common_1.HttpException('Only images are allowed', common_1.HttpStatus.NOT_ACCEPTABLE));
    },
};
//# sourceMappingURL=image-storage.js.map