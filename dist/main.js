"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const validate_pipe_1 = require("./core/database/pipes/validate.pipe");
const swagger_1 = require("@nestjs/swagger");
const express_1 = require("express");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use((0, express_1.json)({ limit: '50mb' }));
    app.use((0, express_1.urlencoded)({ extended: true, limit: '50mb' }));
    app.enableCors();
    app.setGlobalPrefix('api/v1');
    app.useGlobalPipes(new validate_pipe_1.ValidateInputPipe());
    const config = new swagger_1.DocumentBuilder()
        .setTitle('Vallauris')
        .setDescription('Vallauris API ')
        .setVersion('1.0')
        .addBearerAuth()
        .addTag('Vallauris')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api', app, document);
    await app.listen(3000);
}
bootstrap();
//# sourceMappingURL=main.js.map