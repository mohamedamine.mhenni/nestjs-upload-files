import { UsersService } from '../users/users.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { JwtService } from '@nestjs/jwt';
export declare class AuthService {
    private readonly userService;
    private readonly jwtService;
    constructor(userService: UsersService, jwtService: JwtService);
    validateUser(username: string, pass: string): Promise<any>;
    login(user: CreateAuthDto): Promise<{
        user: CreateAuthDto;
        token: string;
    }>;
    verify(token: any): Promise<{
        user: any;
    }>;
    create(user: any): Promise<{
        user: any;
        token: string;
    }>;
    private generateToken;
    private verifyToken;
    private hashPassword;
    private comparePassword;
}
