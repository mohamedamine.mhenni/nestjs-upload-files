import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
export declare class UsersService {
    private readonly userRepository;
    constructor(userRepository: typeof User);
    create(user: CreateUserDto): Promise<User>;
    findOneByEmail(email: string): Promise<User>;
    findOneById(id: number): Promise<User>;
    findAll(): Promise<User[]>;
    update(id: any, data: UpdateUserDto): Promise<{
        numberOfAffectedRows: number;
        updatedUser: User;
    }>;
}
