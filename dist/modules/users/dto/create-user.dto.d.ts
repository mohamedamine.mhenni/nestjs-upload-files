declare enum Gender {
    MALE = "male",
    FEMALE = "female"
}
export declare class CreateUserDto {
    readonly name?: string;
    readonly email?: string;
    readonly password?: string;
    readonly gender?: Gender;
    readonly roles?: string[];
}
export {};
