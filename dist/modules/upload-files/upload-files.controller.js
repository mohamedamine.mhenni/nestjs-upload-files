"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadFilesController = void 0;
const common_1 = require("@nestjs/common");
const upload_files_service_1 = require("./upload-files.service");
const update_upload_file_dto_1 = require("./dto/update-upload-file.dto");
const platform_express_1 = require("@nestjs/platform-express");
const image_storage_1 = require("../../helpers/image-storage");
const swagger_1 = require("@nestjs/swagger");
let UploadFilesController = class UploadFilesController {
    constructor(uploadFilesService) {
        this.uploadFilesService = uploadFilesService;
    }
    async uploadFile(file, req) {
        console.log('the FILE :: ', req.file);
    }
    async uploadMultipleFiles(files, req) {
        console.log('the FILE :: ', files);
    }
    seeUploadedFile(image, res) {
        return res.sendFile(image, { root: './images' });
    }
    findAll() {
        return this.uploadFilesService.findAll();
    }
    findOne(id) {
        return this.uploadFilesService.findOne(+id);
    }
    update(id, updateUploadFileDto) {
        return this.uploadFilesService.update(+id, updateUploadFileDto);
    }
    remove(id) {
        return this.uploadFilesService.remove(+id);
    }
};
__decorate([
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        schema: {
            type: 'object',
            properties: {
                comment: { type: 'string' },
                outletId: { type: 'integer' },
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    }),
    (0, common_1.Post)('upload'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', image_storage_1.saveImageToStorage)),
    __param(0, (0, common_1.UploadedFile)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UploadFilesController.prototype, "uploadFile", null);
__decorate([
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        schema: {
            type: 'object',
            properties: {
                comment: { type: 'string' },
                outletId: { type: 'integer' },
                files: {
                    type: 'array',
                    items: {
                        type: 'string',
                        format: 'binary',
                    },
                },
            },
        },
    }),
    (0, common_1.Post)('upload-multiple'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FilesInterceptor)('files', 10, image_storage_1.saveImageToStorage)),
    __param(0, (0, common_1.UploadedFiles)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, Object]),
    __metadata("design:returntype", Promise)
], UploadFilesController.prototype, "uploadMultipleFiles", null);
__decorate([
    (0, swagger_1.ApiParam)({
        name: 'imgpath',
        required: true,
        description: 'Pass the File name',
        schema: { oneOf: [{ type: 'string' }, { type: 'integer' }] },
    }),
    (0, common_1.Get)(':imgpath'),
    __param(0, (0, common_1.Param)('imgpath')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], UploadFilesController.prototype, "seeUploadedFile", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], UploadFilesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UploadFilesController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_upload_file_dto_1.UpdateUploadFileDto]),
    __metadata("design:returntype", void 0)
], UploadFilesController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UploadFilesController.prototype, "remove", null);
UploadFilesController = __decorate([
    (0, common_1.Controller)('upload-files'),
    __metadata("design:paramtypes", [upload_files_service_1.UploadFilesService])
], UploadFilesController);
exports.UploadFilesController = UploadFilesController;
//# sourceMappingURL=upload-files.controller.js.map