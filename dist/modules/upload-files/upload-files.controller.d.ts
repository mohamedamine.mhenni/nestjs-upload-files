/// <reference types="multer" />
import { UploadFilesService } from './upload-files.service';
import { UpdateUploadFileDto } from './dto/update-upload-file.dto';
export declare class UploadFilesController {
    private readonly uploadFilesService;
    constructor(uploadFilesService: UploadFilesService);
    uploadFile(file: Express.Multer.File, req: any): Promise<void>;
    uploadMultipleFiles(files: Express.Multer.File[], req: any): Promise<void>;
    seeUploadedFile(image: any, res: any): any;
    findAll(): string;
    findOne(id: string): string;
    update(id: string, updateUploadFileDto: UpdateUploadFileDto): string;
    remove(id: string): string;
}
