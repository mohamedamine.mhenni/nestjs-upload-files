import { Model } from 'sequelize-typescript';
export declare class UploadFile extends Model {
    originalName: string;
    fileName: string;
    mimeType: string;
    path: string;
}
