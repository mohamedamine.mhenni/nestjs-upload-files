"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUploadFileDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const create_upload_file_dto_1 = require("./create-upload-file.dto");
class UpdateUploadFileDto extends (0, swagger_1.PartialType)(create_upload_file_dto_1.CreateUploadFileDto) {
}
exports.UpdateUploadFileDto = UpdateUploadFileDto;
//# sourceMappingURL=update-upload-file.dto.js.map