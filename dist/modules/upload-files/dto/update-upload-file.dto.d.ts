import { CreateUploadFileDto } from './create-upload-file.dto';
declare const UpdateUploadFileDto_base: import("@nestjs/common").Type<Partial<CreateUploadFileDto>>;
export declare class UpdateUploadFileDto extends UpdateUploadFileDto_base {
}
export {};
