"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadFilesService = void 0;
const common_1 = require("@nestjs/common");
let UploadFilesService = class UploadFilesService {
    create(createUploadFileDto) {
        return 'This action adds a new uploadFile';
    }
    findAll() {
        return `This action returns all uploadFiles`;
    }
    findOne(id) {
        return `This action returns a #${id} uploadFile`;
    }
    update(id, updateUploadFileDto) {
        return `This action updates a #${id} uploadFile`;
    }
    remove(id) {
        return `This action removes a #${id} uploadFile`;
    }
};
UploadFilesService = __decorate([
    (0, common_1.Injectable)()
], UploadFilesService);
exports.UploadFilesService = UploadFilesService;
//# sourceMappingURL=upload-files.service.js.map