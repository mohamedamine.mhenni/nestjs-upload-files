import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  MinLength,
  Validate,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RolesValidator } from 'src/validators/RolesValidator';
enum Gender {
  MALE = 'male',
  FEMALE = 'female',
}
enum Role {
  USER = 'user',
  ADMIN = 'admin',
}
export class CreateUserDto {
  @ApiProperty({ type: String, description: 'name' })
  @IsNotEmpty()
  readonly name?: string;

  @ApiProperty({ type: String, description: 'username/email' })
  @IsNotEmpty()
  @IsEmail()
  readonly email?: string;

  @ApiProperty({ type: String, description: 'password' })
  @IsNotEmpty()
  @MinLength(6)
  readonly password?: string;

  @ApiProperty({
    oneOf: [
      {
        type: 'array',
        items: {
          type: 'string',
        },
      },
    ],
    description: 'gender male/female',
  })
  @IsNotEmpty()
  @IsEnum(Gender, {
    message: 'gender must be either male or female',
  })
  readonly gender?: Gender;

  @ApiProperty({
    oneOf: [
      {
        type: 'array',
        items: {
          type: 'string',
        },
      },
    ],
    description: "roles['admin','user']",
  })
  @Validate(RolesValidator, ['admin', 'user'])
  readonly roles?: string[];
}
