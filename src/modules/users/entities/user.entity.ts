import { Table, Column, Model, DataType } from 'sequelize-typescript';
import { Role } from 'src/core/database/constants';
@Table({
  timestamps: true,
})
export class User extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: DataType.ENUM,
    values: ['male', 'female'],
    allowNull: false,
  })
  gender: string;

  @Column({
    type: DataType.ARRAY(DataType.STRING),
    defaultValue: ['user'],
    allowNull: false,
  })
  roles: Role[];
}
