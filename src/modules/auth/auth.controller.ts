import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { LocalAuthGuard } from '../../core/database/guards/local-guards';
import { DoesUserExist } from '../../core/database/guards/doesUserExist.guard';
@ApiTags('Authentification')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiCreatedResponse({ description: 'Login service' })
  @ApiBody({ type: CreateAuthDto })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    console.log('IM THE USER THAT LOGGED IN ', req.user);
    return await this.authService.login(req.user);
  }
  @UseGuards(DoesUserExist)
  @Post('signup')
  async signUp(@Body() user: CreateUserDto) {
    return await this.authService.create(user);
  }

  @Get('verify')
  async verify(@Request() req) {
    try {
      const authHeader = req.headers.authorization;
      if (authHeader) {
        const token = authHeader.split(' ')[1];
        const user = await this.authService.verify(token);
        return { status: 200, data: user, message: 'TOKEN_VERIFIED' };
      }
    } catch (e) {
      return { status: 401, error: e.message, message: 'TOKEN_NOT_VERIFIED' };
    }
  }
}
