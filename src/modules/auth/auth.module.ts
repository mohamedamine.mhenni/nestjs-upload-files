import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { RolesGuard } from '../../core/database/guards/roles-guard';
import { JwtAuthGuard } from '../../core/database/guards/jwt-guards';
import { JwtStrategy } from '../../core/database/guards/jwt-strategy';
import { UsersModule } from '../users/users.module';
import { LocalStrategy } from '../../core/database/guards/local-strategy';
import { PassportModule } from '@nestjs/passport';
import { LocalAuthGuard } from '../../core/database/guards/local-guards';

@Module({
  imports: [
    PassportModule,
    forwardRef(() => UsersModule),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: configService.get('TOKEN_EXPIRATION') },
      }),
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    RolesGuard,
    JwtAuthGuard,
    LocalAuthGuard,
    JwtStrategy,
    LocalStrategy,
  ],
  exports: [AuthService],
})
export class AuthModule {}
