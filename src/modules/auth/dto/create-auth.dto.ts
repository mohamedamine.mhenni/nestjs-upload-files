import { IsNotEmpty, MinLength, IsEmail, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateAuthDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ type: String, description: 'username/email' })
  readonly username?: string;

  @IsNotEmpty()
  @MinLength(6)
  @ApiProperty({ type: String, description: 'password' })
  readonly password?: string;
}
