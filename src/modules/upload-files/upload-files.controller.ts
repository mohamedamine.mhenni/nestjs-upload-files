import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Request,
  UploadedFiles,
  Res,
} from '@nestjs/common';
import { UploadFilesService } from './upload-files.service';
import { CreateUploadFileDto } from './dto/create-upload-file.dto';
import { UpdateUploadFileDto } from './dto/update-upload-file.dto';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { saveImageToStorage } from 'src/helpers/image-storage';
import { ApiConsumes, ApiBody, ApiParam } from '@nestjs/swagger';

@Controller('upload-files')
export class UploadFilesController {
  constructor(private readonly uploadFilesService: UploadFilesService) {}

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        comment: { type: 'string' },
        outletId: { type: 'integer' },
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post('upload')
  @UseInterceptors(FileInterceptor('file', saveImageToStorage))
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Request() req) {
    console.log('the FILE :: ', req.file);
  }
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        comment: { type: 'string' },
        outletId: { type: 'integer' },
        files: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  @Post('upload-multiple')
  @UseInterceptors(FilesInterceptor('files', 10, saveImageToStorage))
  async uploadMultipleFiles(
    @UploadedFiles() files: Express.Multer.File[],
    @Request() req,
  ) {
    console.log('the FILE :: ', files);
  }
  @ApiParam({
    name: 'imgpath',
    required: true,
    description: 'Pass the File name',
    schema: { oneOf: [{ type: 'string' }, { type: 'integer' }] },
  })
  @Get(':imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './images' });
  }

  @Get()
  findAll() {
    return this.uploadFilesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.uploadFilesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUploadFileDto: UpdateUploadFileDto,
  ) {
    return this.uploadFilesService.update(+id, updateUploadFileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.uploadFilesService.remove(+id);
  }
}
