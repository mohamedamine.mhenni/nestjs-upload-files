import { Table, Column, Model, DataType } from 'sequelize-typescript';

@Table({
  timestamps: true,
})
export class UploadFile extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  originalName: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  fileName: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  mimeType: string;
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  path: string;
}
